import {DashboardPage} from './DashboardPage';

describe('workspace-project App', () => {
    let page: DashboardPage;

    beforeEach(() => {
        page = new DashboardPage();
    });

    it('Should have Reset or Check Logs button', () => {
        page.navigateTo();
        expect(page.getCheckLogsButton).toBeTruthy();
    });
});
