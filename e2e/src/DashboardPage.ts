import {browser, by, element} from 'protractor';

export class DashboardPage {
    navigateTo() {
        return browser.get('/');
    }

    getResetMachineStateButton() {
        return element(by.cssContainingText('button', 'Reset state machines'));
    }

    getCheckLogsButton() {
        return element(by.cssContainingText('button', 'Check Logs'));
    }


}
