import {TestBed} from '@angular/core/testing';
import {MachineService} from './machine.service';
import {of} from 'rxjs';
import {HttpClientModule} from '@angular/common/http';

describe('MachineService', () => {
    let machineService: MachineService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [MachineService]
        });

        machineService = TestBed.get(MachineService);
    });

    it('should be created', () => {
        expect(machineService).toBeTruthy();
    });

    describe('getSystemCodes', () => {
        it('should return a collection of systemcodes', () => {
            const systemCodesResponse = [
                {
                    code: 'LOT-11'
                }
            ];

            let response;
            spyOn(machineService, 'getSystemCodes').and.returnValue(of(systemCodesResponse));

            machineService.getSystemCodes().subscribe(res => {
                response = res;
            });

            expect(response).toEqual(systemCodesResponse);
        });
    });

    describe('getAllMachines', () => {
        it('should return a collection of machines', () => {
            const machinesResponse = [
                {
                    name: '1001'
                }
            ];

            let response;
            spyOn(machineService, 'getAllMachines').and.returnValue(of(machinesResponse));

            machineService.getAllMachines().subscribe(res => {
                response = res;
            });

            expect(response).toEqual(machinesResponse);
        });
    });
});


