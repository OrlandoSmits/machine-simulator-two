import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MachineLogEntryComponent} from './machine-log-entry.component';
import {MaterialModule} from '../../material.module';

describe('MachineLogEntryComponent', () => {
    let component: MachineLogEntryComponent;
    let fixture: ComponentFixture<MachineLogEntryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MachineLogEntryComponent],
            imports: [MaterialModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MachineLogEntryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
