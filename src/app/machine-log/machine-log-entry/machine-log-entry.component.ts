import {Component, Input, OnInit} from '@angular/core';
import {Log} from '../../model/Log';

@Component({
    selector: 'app-log-entry',
    templateUrl: './machine-log-entry.component.html',
    styleUrls: ['./machine-log-entry.component.scss']
})
export class MachineLogEntryComponent implements OnInit {

    @Input()
    logEntry: Log;

    constructor() {
    }

    ngOnInit() {
    }
}
