import {Component, OnInit} from '@angular/core';
import {MachineService} from '../service/machine/machine.service';
import {Log} from '../model/Log';
import {interval} from 'rxjs';
import {startWith, switchMap} from 'rxjs/operators';
import {PersistenceService, StorageType} from 'angular-persistence';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-machine-log',
    templateUrl: './machine-log.component.html',
    styleUrls: ['./machine-log.component.scss']
})
export class MachineLogComponent implements OnInit {

    logEntries: Log[] = [];

    machineId: string;

    constructor(private machineService: MachineService, private persistenceService: PersistenceService,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.machineId = params['id'];
        });

        interval(5000)
            .pipe(
                startWith(0),
                switchMap(() => this.machineService.getLogsForMachine(this.machineId))
            ).subscribe(
            response => this.addLogs(response)
        );
    }

    addLogs(logs: Log[]) {
        const jsonLogs = this.persistenceService.get(this.machineId, StorageType.SESSION);

        if (jsonLogs != null) {
            this.logEntries = JSON.parse(jsonLogs);
        }

        logs.forEach(log => {
            if (!this.isLogInArray(log)) {
                this.logEntries.push(log);
            }
        });
    }

    private isLogInArray(log: Log): boolean {
        return this.logEntries.some((l) => l.line === log.line);
    }
}
