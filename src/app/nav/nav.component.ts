import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {interval, Observable} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {MachineService} from '../service/machine/machine.service';
import {Machine} from '../model/Machine';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

    machines: Machine[] = [];

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches)
        );

    constructor(private breakpointObserver: BreakpointObserver, private machineService: MachineService) {
    }

    ngOnInit() {
        interval(5000)
            .pipe(
                startWith(0),
                switchMap(() => this.machineService.getAllMachines())
            ).subscribe(
            response => this.addMachines(response)
        );
    }

    addMachines(machines: Machine[]): void {
        machines.forEach(machine => {
            if (!this.isMachineInArray(machine)) {
                this.machines.push(machine);
            }
        });
    }

    private isMachineInArray(machine: Machine): boolean {
        return this.machines.some((m) => m.name === machine.name);
    }
}
