import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MachineLogComponent} from './machine-log/machine-log.component';
import {MachineDashboardComponent} from './machine-dashboard/machine-dashboard.component';

const routes: Routes = [
    {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    {path: 'dashboard', component: MachineDashboardComponent},
    {path: 'logs/:id', component: MachineLogComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
