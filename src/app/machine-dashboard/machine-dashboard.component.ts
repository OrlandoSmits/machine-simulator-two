import {Component, OnInit} from '@angular/core';
import {MachineService} from '../service/machine/machine.service';
import {Machine} from '../model/Machine';
import {Log} from '../model/Log';
import {SystemCode} from '../model/SystemCode';
import {interval} from 'rxjs';
import {startWith, switchMap} from 'rxjs/operators';
import {PersistenceService, StorageType} from 'angular-persistence';
import {Activity} from '../model/Activity';

@Component({
    selector: 'app-machine-dashboard',
    templateUrl: './machine-dashboard.component.html',
    styleUrls: ['./machine-dashboard.component.scss']
})
export class MachineDashboardComponent implements OnInit {

    machinePersistenceKey = 'machines';
    systemCodesPersistenceKey = 'systemCodes';

    machineList: Machine[] = [];
    logList: Log[] = [];
    systemCodeList: SystemCode[] = [];

    constructor(private machineService: MachineService, private persistenceService: PersistenceService) {
    }

    ngOnInit() {
        interval(5000)
            .pipe(
                startWith(0),
                switchMap(() => this.machineService.getSystemCodes())
            ).subscribe(
            systemCodes => this.parseSystemCodes(systemCodes)
        );

        interval(5000)
            .pipe(
                startWith(0),
                switchMap(() => this.machineService.getAllMachines())
            ).subscribe(
            machines => this.parseMachines(machines)
        );
    }


    resetStateAllMachines() {
        this.machineService.resetStateAllMachines().subscribe();
    }


    private parseMachines(machines: Machine[]) {
        const jsonMachines = this.persistenceService.get(this.machinePersistenceKey, StorageType.SESSION);

        if (jsonMachines != null) {
            this.machineList = JSON.parse(jsonMachines);
        }

        machines.forEach(machine => {
            if (!this.isMachineInArray(machine)) {
                this.machineList.push(machine);
            }
            this.machineService.getLogsForMachine(machine.name).subscribe(logs => {
                this.parseLogs(machine, logs);
            });
        });

        if (this.machineList.length !== 0) {
            this.persistenceService.set(this.machinePersistenceKey, JSON.stringify(this.machineList), {type: StorageType.SESSION});
        }
    }

    private parseLogs(machine: Machine, logs: Log[]) {
        logs.forEach(log => {
            const parsedLog = this.parseLog(log);
            this.logList.push(parsedLog);

            if (machine.logList == null) {
                machine.logList = this.logList;
            } else {
                if (!this.isLogInArray(parsedLog)) {
                    machine.logList.push(parsedLog);
                }
            }
            if (this.isLogCodeInSystemCodeArray(parsedLog)) {
                machine.isHealthy = false;
                const systemCode = this.systemCodeList.filter(x => x.code === parsedLog.code)[0];
                this.runActivities(machine, systemCode.activities);
            } else {
                machine.isHealthy = true;
            }
        });

        this.persistenceService.set(this.machinePersistenceKey, JSON.stringify(this.machineList), {type: StorageType.SESSION});

        if (this.logList.length !== 0) {
            this.persistenceService.set(machine.name, JSON.stringify(machine.logList), {type: StorageType.SESSION});
        }
    }

    private runActivities(machine: Machine, activities: Activity[]) {
        activities.forEach(activity => {
            if (activity.exec != null) {
                this.machineService.runActivity(activity.exec).subscribe(response => {
                    this.machineService.getLogsForMachine(machine.name).subscribe(logResponse => {
                        this.parseLogs(machine, logResponse);
                    });

                    if (response.response === 'NOK') {
                        machine.isHealthy = false;
                        this.runActivities(machine, activities);
                    } else if (response.response === 'OK') {
                        machine.isHealthy = true;
                    }
                });
            }
        });
    }

    private parseLog(logEntry: Log): Log {
        if (logEntry != null) {
            logEntry.logDate = logEntry.line.slice(0, 10);
            logEntry.logTime = logEntry.line.slice(11, 19);
            const restString = logEntry.line.slice(19);
            logEntry.code = restString.slice(1, 7);
            logEntry.description = restString.slice(8);

            return logEntry;
        }
    }

    private parseSystemCodes(systemCodes: SystemCode[]) {
        systemCodes.forEach(systemCode => {
            if (!this.isSystemCodeInArray(systemCode)) {
                this.machineService.getActivitiesForSystemCode(systemCode.code).subscribe(response => {
                    systemCode.activities = response;
                });
                this.systemCodeList.push(systemCode);
            }
        });

        this.persistenceService.set(this.systemCodesPersistenceKey, JSON.stringify(this.systemCodeList), {type: StorageType.SESSION});
    }

    private isSystemCodeInArray(systemCode: SystemCode): boolean {
        return this.systemCodeList.some((sc) => sc.code === systemCode.code);
    }

    private isMachineInArray(machine: Machine): boolean {
        return this.machineList.some((m) => m.name === machine.name);
    }

    private isLogInArray(log: Log): boolean {
        return this.logList.some((l) => l.logTime === log.logTime);
    }

    private isLogCodeInSystemCodeArray(log: Log): boolean {
        return this.systemCodeList.some((sc) => sc.code === log.code);
    }
}
