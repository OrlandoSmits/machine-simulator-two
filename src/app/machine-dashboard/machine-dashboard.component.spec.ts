import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MachineDashboardComponent} from './machine-dashboard.component';
import {MachineLogComponent} from '../machine-log/machine-log.component';
import {MachineLogEntryComponent} from '../machine-log/machine-log-entry/machine-log-entry.component';
import {MaterialModule} from '../material.module';
import {MachineService} from '../service/machine/machine.service';
import {HttpClientModule} from '@angular/common/http';
import {MachineDashboardEntryComponent} from './machine-dashboard-entry/machine-dashboard-entry.component';
import {RouterTestingModule} from '@angular/router/testing';
import {PersistenceService} from 'angular-persistence';

describe('MachineDashboardComponent', () => {
    let component: MachineDashboardComponent;
    let fixture: ComponentFixture<MachineDashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MachineDashboardComponent,
                MachineLogComponent,
                MachineLogEntryComponent,
                MachineDashboardEntryComponent],
            imports: [MaterialModule, HttpClientModule, RouterTestingModule],
            providers: [MachineService, PersistenceService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MachineDashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
