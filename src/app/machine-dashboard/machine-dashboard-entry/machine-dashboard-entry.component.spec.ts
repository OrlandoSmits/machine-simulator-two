import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MachineDashboardEntryComponent} from './machine-dashboard-entry.component';
import {PersistenceService} from 'angular-persistence';
import {RouterTestingModule} from '@angular/router/testing';

describe('MachineDashboardEntryComponent', () => {
    let component: MachineDashboardEntryComponent;
    let fixture: ComponentFixture<MachineDashboardEntryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MachineDashboardEntryComponent],
            imports: [RouterTestingModule],
            providers: [PersistenceService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MachineDashboardEntryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
