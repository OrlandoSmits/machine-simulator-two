import {Component, Input, OnInit} from '@angular/core';
import {Machine} from '../../model/Machine';
import {Log} from '../../model/Log';
import {PersistenceService, StorageType} from 'angular-persistence';
import {SystemCode} from '../../model/SystemCode';

@Component({
    selector: 'app-machine-dashboard-entry',
    templateUrl: './machine-dashboard-entry.component.html',
    styleUrls: ['./machine-dashboard-entry.component.scss']
})
export class MachineDashboardEntryComponent implements OnInit {

    @Input()
    machine: Machine;

    lastLog: Log;

    constructor(private persistenceService: PersistenceService) {
    }

    ngOnInit() {
        if (this.machine != null) {
            const jsonData = this.persistenceService.get(this.machine.name, StorageType.SESSION);
            if (jsonData != null) {
                this.machine.logList = JSON.parse(this.persistenceService.get(this.machine.name, StorageType.SESSION));
            }
            this.lastLog = this.getLastLog();
        }
    }

    getLastLog(): Log {
        try {
            return this.machine.logList[this.machine.logList.length - 1];
        } catch (e) {
            return null;
        }
    }

    isMachineHealthy(): boolean {
        if (this.getLastLog() != null) {
            const code = this.getLastLog().code;

            const systemCodesJson = this.persistenceService.get('systemCodes', StorageType.SESSION);
            if (systemCodesJson != null) {
                const systemCodes: SystemCode[] = JSON.parse(systemCodesJson);

                systemCodes.forEach(systemCode => {
                    if (code === systemCode.code) {
                        return false;
                    }
                });
            }

            return code !== 'EXE-11';
        }
    }
}
