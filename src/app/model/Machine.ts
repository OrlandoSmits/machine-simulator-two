import {Log} from './Log';

export class Machine {
    name: string;
    logList: Log[];
    isHealthy: boolean;
}
