export class Log {
    line: string;
    logDate: string;
    logTime: string;
    code: string;
    description: string;
}
